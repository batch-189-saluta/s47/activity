//alert("Hello, 189!");

//querySelector() is a method that can be used to select a specific element from our document
console.log(document.querySelector("#txt-first-name"));
//document refers to the whole page
console.log(document);

/*
	Alternative, that we can use aside from the querySelector in retrieving elements

	document.getElementById("txt-first-name");
	document.getElementsByClassName()
	document.getElementsByTagName()

*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector('#txt-last-name')
const spanFirstName = document.querySelector("#span-first-name");
const spanLastName = document.querySelector("#span-last-name");
console.log(txtFirstName)
console.log(txtLastName)


/*
	Event 
		click, hover, keypress and many other things
	

	Event Listeners
		Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function/task


	Syntax:
		selectedElement.addEventListener('event', function)

*/


/*txtFirstName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = txtFirstName.value

});*/



txtFirstName.addEventListener('keyup', printFirstName)

function printFirstName (event) {

	spanFirstName.innerHTML = txtFirstName.value
}

txtLastName.addEventListener('keyup', printLastName)

function printLastName (event) {
	spanLastName.innerHTML = txtLastName.value
}


// another solution----------------------

const spanFullName = document.querySelector('#span-full-name')

const printFullName = (event) => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener('keyup', printFullName)
txtLastName.addEventListener('keyup', printFullName)





/*txtFirstName.addEventListener('keyup', (event) => {

	console.log(event)
	console.log(event.target)
	console.log(event.target.value)

});*/


/*
	innerHTML - is a property of an element which considers all the children of the selected element as a string.

	.value of the input text field

*/




